package com.example.struct.adapter;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class AdapterTest {
    public static void main(String[] args) {
        Targetable t=new Adapter();
        t.method1();
        t.method2();
    }
}
