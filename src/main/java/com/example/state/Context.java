package com.example.state;

/**
 * Created by weng.junjie on 2017/3/8.
 */
public class Context {
    private State state = null;

    public State getState() {
        return state;
    }

    public void setState(final State state) {
        this.state = state;
    }

    public void push() {
        state.handlePush(this);
    }

    public void pull() {
        state.handlePull(this);
    }
}
