package com.example.state;

/**
 * Created by weng.junjie on 2017/3/8.
 */
public class ConcreteStateC implements State {
    @Override
    public void handlePush(Context context) {
        System.out.println(this.getColor());
    }

    @Override
    public void handlePull(Context context) {
        System.out.println(this.getColor());
        final State state = new ConcreteStateB();
        context.setState(state);
        state.handlePull(context);
    }

    @Override
    public String getColor() {
        return "GREEN";
    }
}
