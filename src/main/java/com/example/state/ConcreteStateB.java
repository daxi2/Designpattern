package com.example.state;

/**
 * Created by weng.junjie on 2017/3/8.
 */
public class ConcreteStateB implements State {
    @Override
    public void handlePush(Context context) {
        System.out.println(this.getColor());
        final State state = new ConcreteStateC();
        context.setState(state);
        state.handlePush(context);
    }

    @Override
    public void handlePull(Context context) {
        System.out.println(this.getColor());
    }

    @Override
    public String getColor() {
        return "YELLOW";
    }
}
