package com.example.state;

/**
 * 状态模式
 * http://www.cnblogs.com/itTeacher/archive/2012/12/04/2801597.html
 */
public interface State {
    public void handlePush(Context context);

    public void handlePull(Context context);

    public String getColor();
}
