package com.example.state;

/**
 * Created by weng.junjie on 2017/3/8.
 */
public class Client {
    public static void main(String[] args) {
        final Context context = new Context();
        context.setState(new ConcreteStateA());
        System.out.println("PUSH");
        context.push();
        System.out.println("pull");
        context.pull();
    }
}
