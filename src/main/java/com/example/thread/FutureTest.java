package com.example.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 本篇说明的是Callable和Future，它俩很有意思的，一个产生结果，一个拿到结果。
 * Created by weng.junjie on 2017/2/16.
 */
public class FutureTest {
    public static void main(String[] args) throws InterruptedException {
        List<Integer> list=new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add(i);
        }
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future future=executor.submit(()->{
            while(list.size()>0){
                System.out.println(list.remove(0));
            }
        });
        //模拟监控多线程是否完成任务？
        System.out.println(future.isDone());
        Thread.sleep(1000);
        System.out.println(future.isDone());
    }
}
