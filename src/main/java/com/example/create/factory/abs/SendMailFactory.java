package com.example.create.factory.abs;

import com.example.create.factory.normal.MailSender;
import com.example.create.factory.normal.Sender;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class SendMailFactory implements Provider{
    @Override
    public Sender produce() {
        return new MailSender();
    }
}
