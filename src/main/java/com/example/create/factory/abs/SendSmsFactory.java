package com.example.create.factory.abs;

import com.example.create.factory.normal.Sender;
import com.example.create.factory.normal.SmsSender;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class SendSmsFactory implements Provider{
    @Override
    public Sender produce() {
        return new SmsSender();
    }
}
