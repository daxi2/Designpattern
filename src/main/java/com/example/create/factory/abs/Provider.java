package com.example.create.factory.abs;

import com.example.create.factory.normal.Sender;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public interface Provider {
    public Sender produce();
}
