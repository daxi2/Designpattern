package com.example.create.factory.sta;

import com.example.create.factory.normal.MailSender;
import com.example.create.factory.normal.Sender;
import com.example.create.factory.normal.SmsSender;

/**
 * 33、静态工厂方法模式，将上面的多个工厂方法模式里的方法置为静态的，不需要创建实例，直接调用即可。
 * Created by weng.junjie on 2017/2/16.
 */
public class SendFactory {
    public static Sender produceMail(){
        return new MailSender();
    }
    public static Sender produceSms(){
        return new SmsSender();
    }
}
