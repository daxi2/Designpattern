package com.example.create.factory.sta;

import com.example.create.factory.normal.Sender;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class FactoryTest {
    public static void main(String[] args) {
        Sender sender=SendFactory.produceMail();
        sender.send();
    }
}
