package com.example.create.factory.multi;

import com.example.create.factory.normal.MailSender;
import com.example.create.factory.normal.Sender;
import com.example.create.factory.normal.SmsSender;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class SendFactory {
    public Sender produceMail(){
        return new MailSender();
    }
    public Sender produceSms(){
        return new SmsSender();
    }
}
