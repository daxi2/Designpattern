package com.example.create.factory.normal;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class SendFactory {
    public Sender produce(String type){
        if("mail".equals(type)){
            return new MailSender();
        }
        else if("sms".equals(type)){
            return new SmsSender();
        }
        else{
            System.out.println("输入类型不正确");
            return null;
        }
    }
}
