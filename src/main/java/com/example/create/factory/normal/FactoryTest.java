package com.example.create.factory.normal;

/**
 * 11、普通工厂模式，就是建立一个工厂类，对实现了同一接口的一些类进行实例的创建。首先看下关系图：
 * Created by weng.junjie on 2017/2/16.
 */
public class FactoryTest {
    public static void main(String[] args) {
        SendFactory f=new SendFactory();//新建一个工厂
        Sender sender=f.produce("sms");//
        sender.send();
    }
}
