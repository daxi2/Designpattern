package com.example.create.factory.normal;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class MailSender implements Sender{
    public MailSender(){
        System.out.println("MailSender init");
    }
    @Override
    public void send() {
        System.out.println("this is mailSend");
    }
}
