package com.example.create.factory.normal;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public interface Sender {
    public void send();
}
