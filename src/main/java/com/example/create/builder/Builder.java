package com.example.create.builder;

import java.util.ArrayList;
import java.util.List;

import com.example.create.factory.normal.MailSender;
import com.example.create.factory.normal.Sender;
import com.example.create.factory.normal.SmsSender;

/**
 * 4、建造者模式（Builder）
 工厂类模式提供的是创建单个类的模式，而建造者模式则是将各种产品集中起来进行管理，用来创建复合对象，所谓复合对象就是指某个类具有不同的属性，
 其实建造者模式就是前面抽象工厂模式和最后的Test结合起来得到的。我们看一下代码：
 * Created by weng.junjie on 2017/2/16.
 */
public class Builder {
    private List<Sender> list=new ArrayList<>();
    public void produceMailSend(int count){
        for(int i=0;i<count;i++){
            list.add(new MailSender());
        }
    }
    public void produceSmsSend(int count){
        for(int i=0;i<count;i++){
            list.add(new SmsSender());
        }
    }
}
