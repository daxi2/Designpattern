package com.example.create.single;

/**
 * 单例模式（Singleton）
 * Created by weng.junjie on 2017/2/16.
 */
public class Singleton {
    /* 持有私有静态实例，防止被引用，此处赋值为null，目的是实现延迟加载 */
    //其次，单例可以被延迟初始化，静态类一般在第一次加载是初始化。之所以延迟加载，是因为有些类比较庞大，所以延迟加载有助于提升性能。
    private static Singleton instance=null;
    /* 私有构造方法，防止被实例化 */
    Singleton(){
        System.out.println("Singleton init");
    }
    /* 此处使用一个内部类来维护单例 */
    private static class SingletonFactory{
        private static Singleton instance=new Singleton();
    }
    /* 静态工程方法，创建实例 */
    //synchronized关键字锁住的是这个对象，这样的用法，在性能上会有所下降，因为每次调用getInstance()，都要对对象上锁，事实上，
    // 只有在第一次创建对象的时候需要加锁，之后就不需要了，所以，这个地方需要改进。
    public static Singleton getInstance(){
//        if(instance==null){
//            synchronized(instance){//变量为null，
//                if(instance==null){
//                    instance=new Singleton();
//                }
//            }
//        }
//        return instance;有bug
        return SingletonFactory.instance;
    }
    /* 如果该对象被用于序列化，可以保证对象在序列化前后保持一致 */
    public Object readResolve(){
        return instance;
    }
    //通过单例模式的学习告诉我们：
    //1、单例模式理解起来简单，但是具体实现起来还是有一定的难度。
    //2、synchronized关键字锁定的是对象，在用的时候，一定要在恰当的地方使用（注意需要使用锁的对象和过程，可能有的时候并不是整个对象及整个过程都需要锁）。
}
