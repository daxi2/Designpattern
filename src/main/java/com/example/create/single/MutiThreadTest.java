package com.example.create.single;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by weng.junjie on 2017/2/16.
 */
public class MutiThreadTest {
    private static ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(2,10,10, TimeUnit.SECONDS,new ArrayBlockingQueue<Runnable>(3));

    public static void main(String[] args) {
        for (int i=0;i<10;i++) {
            // 产生一个任务，并将其加入到线程池
            String task = "task@ " + i;
            threadPoolExecutor.execute(new doData(task));
        }
    }

    private static class doData implements Runnable {
        // 保存任务所需要的数据
        private Object threadPoolTaskData;
        public doData(String task) {
            this.threadPoolTaskData=task;
        }

        @Override
        public void run() {
            //Singleton.getInstance();
            synchronized (doData.class) {
                System.out.println("deal with data " + threadPoolTaskData);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("deal with data ++" + threadPoolTaskData);
            }
        }
    }
}
