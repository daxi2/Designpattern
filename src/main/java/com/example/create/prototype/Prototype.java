package com.example.create.prototype;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 浅复制：将一个对象复制后，基本数据类型的变量都会重新创建，而引用类型，指向的还是原对象所指向的。
 深复制：将一个对象复制后，不论是基本数据类型还有引用类型，都是重新创建的。简单来说，就是深复制进行了完全彻底的复制，而浅复制不彻底。
 * Created by weng.junjie on 2017/2/16.
 */
public class Prototype implements Cloneable,Serializable {
    private static final long serialVersionUId=1;
    private String string;
    private SerializableObject object;
    /* 浅复制 */
    public Object clone() throws  CloneNotSupportedException{
        Prototype prototype=(Prototype) super.clone();
        return prototype;
    }
    public Object deepClone()throws IOException,ClassNotFoundException{
   /* 写入当前对象的二进制流 */
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);

        /* 读出二进制流产生的新对象 */
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        return ois.readObject();
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public SerializableObject getObject() {
        return object;
    }

    public void setObject(SerializableObject object) {
        this.object = object;
    }

    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {
        Prototype i=new Prototype();
        System.out.println(i.toString());
        Object a=i.clone();
        System.out.println(a.toString());
        Object b=i.deepClone();
        System.out.println(b.toString());
    }

    class SerializableObject implements Serializable{
        private static final long serialVersionUID = 1L;
    }
}
